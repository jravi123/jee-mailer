package com.mbcc.mailer;

import java.util.ArrayList;

public interface DataSourceDao {
	
	  boolean insertEmail(EmailDTO email);
	
	  ArrayList<EmailDTO> getEmails();

}
