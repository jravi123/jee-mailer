package com.mbcc.mailer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DerbyDao implements DataSourceDao {


	private static final String DERBY_URL = "jdbc:derby:mailer;create=true";

	public boolean insertEmail(EmailDTO email) {

		try (Connection conn = DriverManager.getConnection(DERBY_URL); Statement stmt = conn.createStatement()) {

			String dbString = "insert into Email (email_text, recipient_category, subject) values ('";
			dbString += email.getEmailText();
			dbString += "', '";
			dbString += email.getRecipientCategory();
			dbString += "', '";
			dbString += email.getSubject();
			dbString += "')";

			stmt.executeUpdate(dbString);

			return true;

		}

		catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			ex.printStackTrace();
			return false;

		}

	}

	public ArrayList<EmailDTO> getEmails() {

		try (Connection conn = DriverManager.getConnection(DERBY_URL); Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery("SELECT * FROM Email");

			ArrayList<EmailDTO> results = new ArrayList<EmailDTO>();

			while (rs.next()) {

				String emailText = rs.getString("email_text");
				String recipientCategory = rs.getString("recipient_category");
				String subject = rs.getString("subject");
				String testerEmail = rs.getString("tester_email");
				EmailDTO email = new EmailDTO(subject, emailText, recipientCategory, testerEmail);
				results.add(email);
			}
			return results;

		} catch (Exception ex) {
			System.out.println(ex);
		}

		return null;

	}

	public void createTable() throws SQLException {

		String emailTableSql = "CREATE TABLE email (" + "  id int NOT NULL GENERATED ALWAYS AS IDENTITY,"
				+ "  email_text blob NOT NULL," + "  recipient_category varchar(255)  NOT NULL,"
				+ "  subject varchar(255)  NOT NULL," + "  tester_email varchar(255) DEFAULT NULL,"
				+ "  email_date timestamp DEFAULT NULL," + "  PRIMARY KEY (id)" + ")";

		Connection conn = DriverManager.getConnection(DERBY_URL);
		Statement stmt = conn.createStatement();
		stmt.execute(emailTableSql);

	}

	
	public static void main(String[] args) throws SQLException {
		DerbyDao dao = new DerbyDao();
		dao.createTable();
	}
}
