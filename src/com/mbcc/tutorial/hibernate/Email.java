package com.mbcc.tutorial.hibernate;

import java.util.Date;
import java.util.Objects;

//@Entity
//@Table(name = "Email")
public class Email {

//	@javax.persistence.Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long Id;

//	@Column(name = "email_text", nullable = false)
	private String emailText;

//	@Column(name = "email_date", nullable = false)
	private Date sentDate;

	public Long getId() {
		return Id;
	}

	public void setId(Long Id) {
		this.Id = Id;
	}

	public String getEmailText() {
		return emailText;
	}

	public void setEmailText(String Name) {
		this.emailText = Name;
	}

	public Date getDate() {
		return sentDate;
	}

	public void setDate(Date date) {
		this.sentDate = date;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Email email = (Email) o;
		return Objects.equals(Id, email.Id) && Objects.equals(emailText, email.emailText)
				&& Objects.equals(sentDate, email.sentDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(Id, emailText, sentDate);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Email{");
		sb.append("Id=").append(Id);
		sb.append(", EmailText='").append(emailText).append('\'');
		sb.append(", Date=").append(sentDate);
		sb.append('}');
		return sb.toString();
	}
}