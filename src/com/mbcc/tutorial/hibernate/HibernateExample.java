package com.mbcc.tutorial.hibernate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

public class HibernateExample {

	private static final String DERBY_URL = "jdbc:derby:test;create=true";

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
    	
    	
    	Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
//    	dropTable();
//    	createTable();
    	
    	

        Email newEmail = new Email();

        newEmail.setEmailText("body of the email- second example");
        newEmail.setDate(new Date());

        EmailService.save(newEmail);

        List<Email> emails = EmailService.getEmails();

        for (Email myemail : emails) {

            System.out.println(myemail);
        }
        
        Long id = 1L;

        Email email = EmailService.getEmailById(id);

        System.out.println(email);

        HibernateUtils.shutdown();
    }
    
    public static void createTable() throws SQLException {

		String emailTableSql = "CREATE TABLE email (" + "  id int NOT NULL GENERATED ALWAYS AS IDENTITY,"
				+ "  email_text varchar(255) NOT NULL," 
				+ "  email_date timestamp DEFAULT NULL," + "  PRIMARY KEY (id)" + ")";

		Connection conn = DriverManager.getConnection(DERBY_URL);
		Statement stmt = conn.createStatement();
		stmt.execute(emailTableSql);

	}
    
    
    public static void dropTable() throws SQLException {
    	Connection conn = DriverManager.getConnection(DERBY_URL);
		Statement stmt = conn.createStatement();
		stmt.execute("drop table email");
    	
    }
	
}
