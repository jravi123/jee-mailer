
package com.mbcc.tutorial.hibernate;

import java.util.List;

import org.hibernate.Session;

public class EmailService {

    private EmailService() {};

    public static Email getEmailById(Long id) {

        Email email;
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            email = session.get(Email.class, id);
        }

        return email;
    }

    @SuppressWarnings("unchecked")
    public static List<Email> getEmails() {

        List<Email> emails;
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            emails = session.createQuery("from Email").list();
        }
        return emails;
    }

    public static void save(Email email) {

        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.beginTransaction();

            session.save(email);

            session.getTransaction().commit();
        }
    }
}
